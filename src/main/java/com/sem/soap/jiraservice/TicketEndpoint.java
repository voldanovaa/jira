package com.sem.soap.jiraservice;

import com.sem.jira_service.GetTicketByIdRequest;
import com.sem.jira_service.GetTicketByIdResponse;
import com.sem.jira_service.TicketInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class TicketEndpoint {

    @Autowired
    TicketService service;

    @PayloadRoot(namespace = "http://sem.soap.com/jira-service", localPart = "GetTicketByIdRequest")
    @ResponsePayload

    public GetTicketByIdResponse processGetTicketByIdRequest(@RequestPayload GetTicketByIdRequest request){

        Ticket ticket = service.findById((int) request.getId());

        return mapTicketInfo(ticket);
    }

    private GetTicketByIdResponse mapTicketInfo(Ticket ticket) {
        GetTicketByIdResponse response = new GetTicketByIdResponse();
        response.setTicketById(mapTicket(ticket));
        return response;
    }


    private TicketInfo mapTicket(Ticket ticket) {
        TicketInfo ticketInfo = new TicketInfo();

        ticketInfo.setId(ticket.getId());
        ticketInfo.setName(ticket.getName());
        ticketInfo.setEmail(ticket.getEmail());
        ticketInfo.setIdCreator(ticket.getIdCreator());
        ticketInfo.setIdAssigned(ticket.getIdAssigned());
        //ticketInfo.setCreationDatetime(ticket.getCreationDatetime());
        //ticketInfo.setTicketCloseDatetime(ticket.getTicketCloseDatetime());

        return ticketInfo;

    }

}










