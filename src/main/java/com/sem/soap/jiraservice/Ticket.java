package com.sem.soap.jiraservice;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;

public class Ticket {

    private long id;
    private String name;
    private String email;
    private long idCreator;
    private long idAssigned;
    //private XMLGregorianCalendar creationDatetime;
    //private XMLGregorianCalendar ticketCloseDatetime;

    public Ticket(long id, String name, String email,
                  long idCreator, long idAssigned) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.idCreator = idCreator;
        this.idAssigned = idAssigned;
        //this.creationDatetime = creationDatetime;
        //this.ticketCloseDatetime = ticketCloseDatetime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getIdCreator() {
        return idCreator;
    }

    public void setIdCreator(long idCreator) {
        this.idCreator = idCreator;
    }

    public long getIdAssigned() {
        return idAssigned;
    }

    public void setIdAssigned(long idAssigned) {
        this.idAssigned = idAssigned;
    }

//    public XMLGregorianCalendar getCreationDatetime() {
//        return creationDatetime;
//    }
//
//    public void setCreationDatetime(XMLGregorianCalendar creationDatetime) {
//        this.creationDatetime = creationDatetime;
//    }
//
//    public XMLGregorianCalendar getTicketCloseDatetime() {
//        return ticketCloseDatetime;
//    }
//
//    public void setTicketCloseDatetime(XMLGregorianCalendar ticketCloseDatetime) {
//        this.ticketCloseDatetime = ticketCloseDatetime;
//    }


    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", idCreator=" + idCreator +
                ", idAssigned=" + idAssigned +
                '}';
    }
}
