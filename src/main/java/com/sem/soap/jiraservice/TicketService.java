package com.sem.soap.jiraservice;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class TicketService {

    private static List<Ticket> tickets = new ArrayList<>();

    static {
        Ticket ticket1 = new Ticket(1, "name1", "name1@email.com",5,6);
        tickets.add(ticket1);

        Ticket ticket2 = new Ticket(2, "name2", "name2@email.com", 6,5);
        tickets.add(ticket2);

    }


    public Ticket findById(int id){
        for(Ticket ticket:tickets){
            if(ticket.getId()==id) {
                return ticket;
            }
        }
        return null;
    }
}
