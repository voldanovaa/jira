package com.sem.soap.jiraservice;


import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class TicketConfig {

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();

        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);

        return new ServletRegistrationBean(messageDispatcherServlet, "/ws/*");
    }

    // /ws/tickets.wsdl
    //PortType - CoursePort
    //Namespace - http://in28minutes.com/tickets
    //jira-service.xsd

    //defining the schema
    @Bean(name="tickets")
    public DefaultWsdl11Definition defaultWsdl11Definition (XsdSchema coursesSchema){

        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();

        definition.setPortTypeName("CoursePort");
        definition.setTargetNamespace("http://in28minutes.com/courses");
        definition.setLocationUri("/ws");
        definition.setSchema(coursesSchema);

        return definition;
    }

    @Bean
    public XsdSchema ticketsSchema(){

        return new SimpleXsdSchema(new ClassPathResource("jira-service.xsd"));
    }


}
